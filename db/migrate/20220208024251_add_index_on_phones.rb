class AddIndexOnPhones < ActiveRecord::Migration[6.1]
  def change
    add_index :phones, %i[contact_id number], unique: true
  end
end
