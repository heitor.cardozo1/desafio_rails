class Phone < ApplicationRecord
  validates :number, presence: true, uniqueness: { scope: :contact_id }
  validates :label, presence: true,
                    inclusion: { in: %w[home work other], message: 'Invalid option. Label valid options are: home, work or other.' }
  validates :default, inclusion: [true, false]
  before_save :change_default, if: :default?

  belongs_to :contact

  private

  def change_default
    old_default = Phone.where(default: true, contact_id:)
    old_default.update(default: false)
  end
end
