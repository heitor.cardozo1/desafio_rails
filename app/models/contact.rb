class Contact < ApplicationRecord
  validates :name, presence: true, uniqueness: { scope: :user_id }
  validates_associated :phones

  belongs_to :user
  has_many :phones
end
