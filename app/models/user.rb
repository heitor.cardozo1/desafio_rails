class User < ApplicationRecord
  validates :name, presence: true, uniqueness: true
  validates_associated :contacts
  validates_associated :phones

  has_many :contacts
  has_many :phones, through: :contacts
end
